import os, glob
import nltk

from bs4 import BeautifulSoup

import string
import functools
import re
p = re.compile('[а-я]')

from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

import pymorphy2

# ----------------------------
class Document:
    def __init__(self, number, text, tokens, lemmas):
        self.text = text
        self.tokens = tokens
        self.lemmas = lemmas
        self.number = number
        
# ----------------------------
PAGES_FOLDER = '../pages/'

morph = pymorphy2.MorphAnalyzer()
lemmas = {}
tokens = set()
documents = []
lemma_doc_numbers = {}
# ----------------------------
def filename_cmp(a, b):
    if len(a) != len(b): return len(a) - len(b)
    return (a > b) - (a < b) 

# Токенизация строки
def tokenize_ru(file_text):
    result_tokens = []
    # Применяем токенизачию из nltk  
    tokens = word_tokenize(file_text.lower())

    # Удаляем стоп слова
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', '—', '–', 'к', 'на', '...'])
    for token in tokens:
        if token not in stop_words and p.match(token) and len(token) > 1:
            result_tokens.append(token)
    return result_tokens

def lemmatize(tokens):
    result = []
    for token in tokens:
        normal_form = morph.parse(token)[0].normal_form
        result.append(normal_form)
        if normal_form not in lemmas:
            lemmas[normal_form] = set()
        lemmas[normal_form].add(token)
    return result
# ----------------------------

# Достаем текст из каждой html страницы и выполняем действия:
for file_path in sorted(os.listdir(PAGES_FOLDER), key=functools.cmp_to_key(filename_cmp)):
    if not file_path.endswith('html'): continue
    html_page = open(PAGES_FOLDER + file_path, 'r').read()
    soup_page = BeautifulSoup(html_page, features="html.parser")
    # Получаем текст, выполняем токенизацию и лемматизацию
    text = soup_page.get_text()
    tokens_set = set(tokenize_ru(text))
    lemmas_set = set(lemmatize(tokens_set))
    tokens.update(tokens_set)
    # Создаем объект документа со всеми данными документа     
    document = Document(int(file_path.split('.')[0]), text, tokens_set, lemmas_set)
    documents.append(document)
    
# Записываем токены в файл
with open('../tokens.txt', 'w') as tokens_file:
    for token in tokens:
        tokens_file.write(token + '\n')
    tokens_file.close()
    
# Записываем леммы с токенами в файл
with open('../lemmas.txt', 'w') as lemmas_file:
    for lemma in lemmas:
        lemmas_file.write(lemma)
        for token in lemmas[lemma]:
            lemmas_file.write(' ' + token)
        lemmas_file.write('\n')
    lemmas_file.close()
    

# Проходим по документам и формируем инвертированный индекс
for doc in documents:
    for lemma in doc.lemmas:
        if lemma not in lemma_doc_numbers:
            lemma_doc_numbers[lemma] = []
        lemma_doc_numbers[lemma].append(doc.number)

# Записываем индекс в файл
with open('../inverted_index.txt', 'w') as inverted_index_file:
    for lemma in lemma_doc_numbers:
        inverted_index_file.write(lemma)
        for doc_num in lemma_doc_numbers[lemma]:
            inverted_index_file.write(' ' + str(doc_num))
        inverted_index_file.write('\n')
        
# ----------------------------
''' 
Любое булевое выражение можно записать в виде КНФ, 
следовательно, булевый поиск будет работать для всевозможных буевых выражений.

Записываем условие в виде КНФ но без скобок
'''
query="NOT палуба AND убивать OR курбатов"

result_set = set([doc.number for doc in documents])
total_set = result_set.copy()

# Разбиваем на отдельные части только с OR
for or_part in query.split("AND"):
    or_part = or_part.strip()
    
    or_part_set = set()
    # Разбиваем на единичные части
    for unit in or_part.split("OR"):
        unit = unit.strip()
        if 'NOT ' in unit:
            # Если отрицание, то вычитаем из полного набора документов              
            or_part_set |= (total_set - set(lemma_doc_numbers[unit[4:]]))
        else:
            # Иначе берем только документы с этим словом
            or_part_set |= set(lemma_doc_numbers[unit])

    result_set &= or_part_set

print(result_set)