import os, glob
import nltk
import re 
p = re.compile("[а-я]")

from bs4 import BeautifulSoup

import string

from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

import pymorphy2
morph = pymorphy2.MorphAnalyzer()

PAGES_FOLDER = '../pages/'

print('Starting...\n')

# Токенизация строки
def tokenize_ru(file_text):
    result_tokens = []
    # Применяем токенизачию из nltk  
    tokens = word_tokenize(file_text.lower())

    # Удаляем стоп слова
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', '—', '–', 'к', 'на', '...'])
    for token in tokens:
        if token not in stop_words and p.match(token) and len(token) > 1:
            result_tokens.append(token)
    return result_tokens

tokens = set()

# Достаем текст из каждой html страницы и выполняем токенизацию
for file_path in os.listdir(PAGES_FOLDER):
    if not file_path.endswith('html'): continue
    print('\r' + file_path)
    html_page = open(PAGES_FOLDER + file_path, 'r').read()
    soup_page = BeautifulSoup(html_page, features="html.parser")
    tokens.update(tokenize_ru(soup_page.get_text()))
    
tokens = sorted(tokens)

lemmas = {}

# Выполняем лемматизацию с помощью pymorphy2.MorphAnalyzer
for token in tokens:
    normal_form = morph.parse(token)[0].normal_form
    if normal_form not in lemmas:
        lemmas[normal_form] = set()
    lemmas[normal_form].add(token)
    
# Записываем токены в файл
with open('../tokens.txt', 'w') as tokens_file:
    for token in tokens:
        tokens_file.write(token + '\n')
    tokens_file.close()
    
# Записываем леммы с токенами в файл
with open('../lemmas.txt', 'w') as lemmas_file:
    for lemma in lemmas:
        lemmas_file.write(lemma)
        for token in lemmas[lemma]:
            lemmas_file.write(' ' + token)
        lemmas_file.write('\n')
    lemmas_file.close()