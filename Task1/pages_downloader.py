import requests

file = open('links.txt', 'r')
index = open('../index.txt', 'w')
lines = file.readlines();
for i, link in enumerate(lines):
    r = requests.get(link.strip())
    index.write(str(i) + ':' + link)
    with open('../pages/' + str(i) + '.html', 'w') as out:
        out.write(r.text)
        out.close()
index.close()