import collections, os, glob, nltk, string, math, functools

from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from bs4 import BeautifulSoup
import re
p = re.compile('[а-я]')

import pymorphy2
morph = pymorphy2.MorphAnalyzer()

PAGES_FOLDER = '../pages/'

# ---------- Утилитарные функции ----------
def filename_cmp(a, b):
    if len(a) != len(b): return len(a) - len(b)
    return (a > b) - (a < b)

# Токенизация строки
def tokenize_ru(file_text):
    result_tokens = []
    # Применяем токенизачию из nltk  
    tokens = word_tokenize(file_text.lower())

    # Удаляем стоп слова
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', '—', '–', 'к', 'на', '...'])
    for token in tokens:
        if token not in stop_words and p.match(token) and len(token) > 1:
            result_tokens.append(token)
    return result_tokens

# ---------- Функции калькуляции ----------
def compute_tf(text):
    #На вход берем текст в виде списка (list) слов
    #Считаем частотность всех терминов во входном массиве с помощью 
    #метода Counter библиотеки collections
    tf_text = collections.Counter(text)
    for i in tf_text:
        #для каждого слова в tf_text считаем TF путём деления
        #встречаемости слова на общее количество слов в тексте
        tf_text[i] = tf_text[i]/float(len(text))
    #возвращаем объект типа Counter c TF всех слов текста
    return tf_text

def compute_idfs(words, corpus):
    idf = dict()
    for word in words:
        idf[word] = math.log10(len(corpus) / sum([1.0 for i in corpus if word in i]))
    return idf

def compute_idf(word, corpus):
    #на вход берется слово, для которого считаем IDF
    #и корпус документов в виде списка списков слов
    #количество документов, где встречается искомый термин
    #считается как генератор списков
    return math.log10(len(corpus)/sum([1.0 for i in corpus if word in i]))

def compute_tfidf(corpus, idfs, folder):
    for i, text in enumerate(corpus):
        computed_tf = compute_tf(text)
        
        with open('../{}/{}.txt'.format(folder, i), 'w') as tfidf_file:
            for word in computed_tf:
                idf = idfs[word]
                tf = computed_tf[word]
                tfidf_file.write('{} {} {}\n'.format(word, idf, tf * idf))
            tfidf_file.close()
        
# ---------- Основной код ----------
# Массивы для массивов лемм и токенов по каждому документу
corpus_tokens = []
corpus_lemmas = []
lemmas = set()
tokens = set()

for file_path in sorted(os.listdir(PAGES_FOLDER), key=functools.cmp_to_key(filename_cmp)):
    if not file_path.endswith('html'): continue
    print(file_path)
    html_page = open(PAGES_FOLDER + file_path, 'r').read()
    soup_page = BeautifulSoup(html_page, features="html.parser")
    doc_tokens = tokenize_ru(soup_page.get_text())
    # Добавляем в корпус токены каждого документа     
    corpus_tokens.append(doc_tokens)
    
    doc_lemmas = set()
    for token in doc_tokens:
        normal_form = morph.parse(token)[0].normal_form
        doc_lemmas.add(normal_form)
    # Добавляем в другой корпус леммы каждого документы
    corpus_lemmas.append(list(doc_lemmas))
    lemmas.update(doc_lemmas)
    tokens.update(doc_tokens)

# tokens_idf = compute_idfs(tokens, corpus_tokens) 
lemmas_idf = compute_idfs(lemmas, corpus_lemmas) 
# Считаем и записываем в файлы tf-idf для токенов и для лемм
# compute_tfidf(corpus_tokens, tokens_idf, folder='tfidf_tokens')
# compute_tfidf(corpus_lemmas, lemmas_idf, folder='tfidf_lemmas')


lemmas_idf_file = open("../lemmas_idf.txt", "w")
for lemma in lemmas_idf:
    lemmas_idf_file.write(f'{lemma} {lemmas_idf[lemma]}\n')
lemmas_idf_file.close()