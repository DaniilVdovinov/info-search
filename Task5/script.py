import sys, math
import numpy as np
import os, glob
import nltk
import string
import functools
import re
p = re.compile('[а-я]')

from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

import pymorphy2
import collections
from scipy import spatial

from PyQt6.QtCore import QSize, Qt
from PyQt6.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QLabel,
    QLineEdit,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QFormLayout,
    QCheckBox,
    QGridLayout
)

morph = pymorphy2.MorphAnalyzer()

def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)

# Токенизация строки
def tokenize_ru(file_text):
    result_tokens = []
    # Применяем токенизачию из nltk  
    tokens = word_tokenize(file_text.lower())

    # Удаляем стоп слова
    stop_words = stopwords.words('russian')
    stop_words.extend(['что', 'это', 'так', 'вот', 'быть', 'как', 'в', '—', '–', 'к', 'на', '...'])
    for token in tokens:
        if token not in stop_words and p.match(token) and len(token) > 1:
            result_tokens.append(token)
    return result_tokens

def lemmatize(query):
    tokens = tokenize_ru(query)
    result = []
    for token in tokens:
        normal_form = morph.parse(token)[0].normal_form
        result.append(normal_form)
    return result

def read_inv_index():
    inv_index_file = open('../inverted_index.txt', 'r')
    inv_index = dict()
    lemmas = []
    for line in inv_index_file.readlines():
        split = line.replace('\n', '').split(' ')
        inv_index[split[0]] = [int(i) for i in split[1:]]
        lemmas.append(split[0])
    return lemmas, inv_index

def read_idf():
    lemmas = set()
    file = open('../lemmas_idf.txt', 'r')
    idf = dict()
    for line in file.readlines():
        lemma, _idf = line.replace('\n', '').split(' ')
        idf[lemma] = float(_idf)
    return idf;

def load_matrix(total_lemmas, inv_index):
    matrix = np.zeros((122, len(inv_index)))
    
    for path in os.listdir('../tfidf_lemmas/'):
        if not path.endswith('txt'): continue
        doc_idf = dict()
        doc_tfidf = dict()
        doc_lemmas = []
        doc_num = int(path.split('.')[0])
        
        for lemma_line in open('../tfidf_lemmas/' + path).readlines():
            lemma, idf, tfidf = lemma_line.replace('\n', '').split(' ')
            doc_lemmas.append(lemma)
            doc_idf[lemma] = float(idf)
            doc_tfidf[lemma] = float(tfidf)
        
        for lemma in doc_lemmas:
            matrix[doc_num - 1][total_lemmas.index(lemma)] = doc_tfidf[lemma]
            
    return matrix

def get_doc_urls():
    doc_num_url = dict()
    file = open('../index.txt')
    for line in file.readlines():
        line = line.replace('\n', '')
        index = line.find(':')
        doc_num_url[int(line[:index])] = line[index+1:]
    return doc_num_url

print("LOADING")
doc_num_url = get_doc_urls()
    
lemmas, index = read_inv_index()
matrix = load_matrix(lemmas, index)

def search(query):
    search_lemmas = lemmatize(query.lower())
    
    idf = read_idf()
    
    words_frequency = collections.Counter(search_lemmas)

    vector = np.zeros(len(index))
    for lemma in index:
        if lemma in search_lemmas:
            vector[lemmas.index(lemma)] = (words_frequency[lemma] / float(len(index[lemma]))) * idf[lemma]
            
    texts = dict()
    for idx, row in enumerate(matrix):
        texts[idx + 1] = 1 - spatial.distance.cosine(vector, row)

    texts_sorted = dict(sorted(texts.items(), key=lambda item: item[1], reverse=True)[:10])
    return texts_sorted

# Подкласс QMainWindow для настройки главного окна приложения
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("My App")

        self.grid = QGridLayout()
        self.grid.setSpacing(10)
        
        self.grid.addWidget(QLabel("Введи поисковый запрос"), 1, 0)
        self.input = QLineEdit()
        self.grid.addWidget(self.input, 2, 0)
        
        self.button = QPushButton("Поиск!")
        self.button.clicked.connect(self.handle_button_click)
        self.grid.addWidget(self.button, 2, 1)
        
        self.init_list()
        
        container = QWidget()
        container.setLayout(self.grid)
        self.setCentralWidget(container)
    
    def init_list(self):
        self.list = []
        for i in range(10):
            doc = QLabel('')
            doc.setOpenExternalLinks(True)
            num = QLabel('')
            self.list.append([doc, num])
            self.grid.addWidget(doc, 3 + i, 0)
            self.grid.addWidget(num, 3 + i, 1)
        
    def handle_button_click(self):
        for i, item in enumerate(search(self.input.text()).items()):
            self.list[i][0].setText(f"<a href='{doc_num_url[item[0]]}'>{item[0]}</a>")
            self.list[i][1].setText(str(item[1]))

app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()